/**
 * Module Dependencies
 */
var express = require('express'),
    morgan = require('morgan'),
    bodyParser = require('body-parser'),
    methodOverride = require('method-override'),
    config = require('./config/conf'),
    channel = config.irc.options.channels[0],
    irc = require('irc');
    
/**
 * IRC connection/bindings
 */
var client = new irc.Client(config.irc.server, config.irc.nick, config.irc.options);
client.addListener('error', function(message) {
  console.log('error: '+message);
});

/**
 * Express
 */
var app = express();


/**
 * Stack
 */
app.enable('trust proxy');
app.set('port', process.env.PORT || 3000);
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');
app.use(morgan('dev'));
app.use(bodyParser({limit: '50mb', extended: true}));
app.use(methodOverride());
//app.use(express.static(__dirname + '/public'));

/**
 * Routers
 */
var mainRouter = express.Router();

/**
 * Main Router
 */
// POST HOOK FROM BITBUCKET
mainRouter.post("/", function(req, res) {
  var hook = JSON.parse(req.body.payload);
  
  if (hook) {
    
    client.say(channel, hook.repository.name+': '+hook.commits.length+' new commits pushed by '+hook.user);
    
    for (var a = 0; a < hook.commits.length; a++) {
      var commit = hook.commits[a],
          author = commit.author,
          branch = commit.branch,
          message = commit.message,
          node = commit.node,
          files = commit.files;
      
      message = message.replace(/(\r\n|\n|\r)/gm,"");
      
      client.say(channel, '------------------------------------');
      client.say(channel, '  Commit: '+node);
      client.say(channel, '  Message: "'+message+'"');
      client.say(channel, '  Author: '+author);
      client.say(channel, '  Branch: '+branch);
      client.say(channel, '  Files: '+files.length);
      
      for (var b = 0; b < files.length; b++) {
        client.say(channel, '    '+files[b].type+' '+files[b].file);
      }
      client.say(channel, '------------------------------------');
      
    }
    res.send(200);
  }
  else {
    res.send(500);
  }
});


app.use("/", mainRouter);

app.listen(app.get('port'), function() {
  console.log("Express server listening on port %d in %s mode", app.get('port'), app.settings.env);
});