module.exports = {
  irc: {
    server: 'irc.freenode.net',
    nick: 'tessa_git',
    options: {
      channels: ['#expimetrics'],
      userName: 'CommitBot',
      realName: 'Tessa GIT commit bot',
      port: 6697,
      secure: true,
      selfSigned: true,
      certExpired: true,
      debug: false,
      floodProtection: true,
      floodProtectionDelay: 1000,
    }
    
  }
};